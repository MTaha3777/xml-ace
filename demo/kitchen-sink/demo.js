define(function (require) {
    "use strict";
    /************************Environment object of xml editor*************************************/
    var envXmlContainer = {};
    /************************Environment object of beautifier editor*************************************/
    var envBeautifierContainer = {};

    /************************Environment object of beautifier editor*************************************/
    var envTreeContainer = {};

    /************************Theme for editor*************************************/
    var theme = require("ace/theme/textmate");

    var Split = require("ace/split").Split;
    /****************************************************************
     create beautifier editor
     ****************************************************************/

    var xmlBeautifier = document.getElementById("xml-beautifier");
    var splitBeautifierContainer = new Split(xmlBeautifier, theme, 1);
    envBeautifierContainer.editor = splitBeautifierContainer.getEditor(0);
    envBeautifierContainer.editor.setReadOnly(true);

    /****************************************************************
     create xml editor
     ****************************************************************/
    var container = document.getElementById("xml-code-editor");
    var split = new Split(container, theme, 1);
    envXmlContainer.editor = split.getEditor(0);




    /**********************************************************************************
     Beautifier code and xml code set by Default start and mode set to xml
     **********************************************************************************/
    envBeautifierContainer.editor.setValue("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
        "<query xmlns:yahoo=\"http://www.yahooapis.com/v1/base.rng\"\n" +
        "       yahoo:count=\"7\" yahoo:created=\"2011-10-11T08:40:23Z\" yahoo:lang=\"en-US\">\n" +
        "    <diagnostics>\n" +
        "        <publiclyCallable>true</publiclyCallable>\n" +
        "        <url execution-start-time=\"0\" execution-stop-time=\"25\" execution-time=\"25\"><![CDATA[http://where.yahooapis.com/v1/continents;start=0;count=10]]></url>\n" +
        "        <user-time>26</user-time>\n" +
        "        <service-time>25</service-time>\n" +
        "        <build-version>21978</build-version>\n" +
        "    </diagnostics>\n" +
        "    <results>\n" +
        "        <place xmlns=\"http://where.yahooapis.com/v1/schema.rng\"\n" +
        "               xml:lang=\"en-US\" yahoo:uri=\"http://where.yahooapis.com/v1/place/24865670\">\n" +
        "            <woeid>24865670</woeid>\n" +
        "            <placeTypeName code=\"29\">Continent</placeTypeName>\n" +
        "            <name>Africa</name>\n" +
        "        </place>\n" +
        "        <place xmlns=\"http://where.yahooapis.com/v1/schema.rng\"\n" +
        "               xml:lang=\"en-US\" yahoo:uri=\"http://where.yahooapis.com/v1/place/24865675\">\n" +
        "            <woeid>24865675</woeid>\n" +
        "            <placeTypeName code=\"29\">Continent</placeTypeName>\n" +
        "            <name>Europe</name>\n" +
        "        </place>\n" +
        "        <place xmlns=\"http://where.yahooapis.com/v1/schema.rng\"\n" +
        "               xml:lang=\"en-US\" yahoo:uri=\"http://where.yahooapis.com/v1/place/24865673\">\n" +
        "            <woeid>24865673</woeid>\n" +
        "            <placeTypeName code=\"29\">Continent</placeTypeName>\n" +
        "            <name>South America</name>\n" +
        "        </place>\n" +
        "        <place xmlns=\"http://where.yahooapis.com/v1/schema.rng\"\n" +
        "               xml:lang=\"en-US\" yahoo:uri=\"http://where.yahooapis.com/v1/place/28289421\">\n" +
        "            <woeid>28289421</woeid>\n" +
        "            <placeTypeName code=\"29\">Continent</placeTypeName>\n" +
        "            <name>Antarctic</name>\n" +
        "        </place>\n" +
        "        <place xmlns=\"http://where.yahooapis.com/v1/schema.rng\"\n" +
        "               xml:lang=\"en-US\" yahoo:uri=\"http://where.yahooapis.com/v1/place/24865671\">\n" +
        "            <woeid>24865671</woeid>\n" +
        "            <placeTypeName code=\"29\">Continent</placeTypeName>\n" +
        "            <name>Asia</name>\n" +
        "        </place>\n" +
        "        <place xmlns=\"http://where.yahooapis.com/v1/schema.rng\"\n" +
        "               xml:lang=\"en-US\" yahoo:uri=\"http://where.yahooapis.com/v1/place/24865672\">\n" +
        "            <woeid>24865672</woeid>\n" +
        "            <placeTypeName code=\"29\">Continent</placeTypeName>\n" +
        "            <name>North America</name>\n" +
        "        </place>\n" +
        "        <place xmlns=\"http://where.yahooapis.com/v1/schema.rng\"\n" +
        "               xml:lang=\"en-US\" yahoo:uri=\"http://where.yahooapis.com/v1/place/55949070\">\n" +
        "            <woeid>55949070</woeid>\n" +
        "            <placeTypeName code=\"29\">Continent</placeTypeName>\n" +
        "            <name>Australia</name>\n" +
        "        </place>\n" +
        "    </results>\n" +
        "</query>")

    envXmlContainer.editor.setValue("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
        "<query xmlns:yahoo=\"http://www.yahooapis.com/v1/base.rng\"\n" +
        "       yahoo:count=\"7\" yahoo:created=\"2011-10-11T08:40:23Z\" yahoo:lang=\"en-US\">\n" +
        "    <diagnostics>\n" +
        "        <publiclyCallable>true</publiclyCallable>\n" +
        "        <url execution-start-time=\"0\" execution-stop-time=\"25\" execution-time=\"25\"><![CDATA[http://where.yahooapis.com/v1/continents;start=0;count=10]]></url>\n" +
        "        <user-time>26</user-time>\n" +
        "        <service-time>25</service-time>\n" +
        "        <build-version>21978</build-version>\n" +
        "    </diagnostics>\n" +
        "    <results>\n" +
        "        <place xmlns=\"http://where.yahooapis.com/v1/schema.rng\"\n" +
        "               xml:lang=\"en-US\" yahoo:uri=\"http://where.yahooapis.com/v1/place/24865670\">\n" +
        "            <woeid>24865670</woeid>\n" +
        "            <placeTypeName code=\"29\">Continent</placeTypeName>\n" +
        "            <name>Africa</name>\n" +
        "        </place>\n" +
        "        <place xmlns=\"http://where.yahooapis.com/v1/schema.rng\"\n" +
        "               xml:lang=\"en-US\" yahoo:uri=\"http://where.yahooapis.com/v1/place/24865675\">\n" +
        "            <woeid>24865675</woeid>\n" +
        "            <placeTypeName code=\"29\">Continent</placeTypeName>\n" +
        "            <name>Europe</name>\n" +
        "        </place>\n" +
        "        <place xmlns=\"http://where.yahooapis.com/v1/schema.rng\"\n" +
        "               xml:lang=\"en-US\" yahoo:uri=\"http://where.yahooapis.com/v1/place/24865673\">\n" +
        "            <woeid>24865673</woeid>\n" +
        "            <placeTypeName code=\"29\">Continent</placeTypeName>\n" +
        "            <name>South America</name>\n" +
        "        </place>\n" +
        "        <place xmlns=\"http://where.yahooapis.com/v1/schema.rng\"\n" +
        "               xml:lang=\"en-US\" yahoo:uri=\"http://where.yahooapis.com/v1/place/28289421\">\n" +
        "            <woeid>28289421</woeid>\n" +
        "            <placeTypeName code=\"29\">Continent</placeTypeName>\n" +
        "            <name>Antarctic</name>\n" +
        "        </place>\n" +
        "        <place xmlns=\"http://where.yahooapis.com/v1/schema.rng\"\n" +
        "               xml:lang=\"en-US\" yahoo:uri=\"http://where.yahooapis.com/v1/place/24865671\">\n" +
        "            <woeid>24865671</woeid>\n" +
        "            <placeTypeName code=\"29\">Continent</placeTypeName>\n" +
        "            <name>Asia</name>\n" +
        "        </place>\n" +
        "        <place xmlns=\"http://where.yahooapis.com/v1/schema.rng\"\n" +
        "               xml:lang=\"en-US\" yahoo:uri=\"http://where.yahooapis.com/v1/place/24865672\">\n" +
        "            <woeid>24865672</woeid>\n" +
        "            <placeTypeName code=\"29\">Continent</placeTypeName>\n" +
        "            <name>North America</name>\n" +
        "        </place>\n" +
        "        <place xmlns=\"http://where.yahooapis.com/v1/schema.rng\"\n" +
        "               xml:lang=\"en-US\" yahoo:uri=\"http://where.yahooapis.com/v1/place/55949070\">\n" +
        "            <woeid>55949070</woeid>\n" +
        "            <placeTypeName code=\"29\">Continent</placeTypeName>\n" +
        "            <name>Australia</name>\n" +
        "        </place>\n" +
        "    </results>\n" +
        "</query>")



    envXmlContainer.editor.session.setMode("ace/mode/xml")
    envBeautifierContainer.editor.session.setMode("ace/mode/xml")



    // /*******************************************************************
    //  Xml to Minified Function Work
    //  *******************************************************************/
    //
    // var xmlTreeMinifiedEditor = document.getElementById("minifiedButton")
    //
    // xmlTreeMinifiedEditor.onclick = function (e) {
    //     // debugger
    //     // console.log("Minified")
    //     var totalLineNumber=envXmlContainer.editor.session.getLength();
    //     var result  = envXmlContainer.editor.getValue();
    //     var minifiedXml = result.replace(/\n|\r/g, " ");
    //     var removeSpaces=minifiedXml.replace(/\s/g, '');
    //
    //     envBeautifierContainer.editor.setValue(removeSpaces)
    //     // console.log(removeSpaces)
    //
    // }



    /*******************************************************************
     Xml to Beautifier Function Work
     *******************************************************************/

    var xmlTreeEditor = document.getElementById("beautifierButton")
    var XmlBeautify = require("./XmlBeautify")
    xmlTreeEditor.onclick = function (e) {
        // debugger


        /*
        get the content of editor xml
         */

        var xmlCode = envXmlContainer.editor.getValue()

        /*
        beautify the content with our js file
         */

        var prettyXmlText = new XmlBeautify().beautify(xmlCode,
            {indent: "    ",useSelfClosingElement: false});

        /*
       set the content after beautify in beautify editor
         */
        envBeautifierContainer.editor.setValue(prettyXmlText)

    }



    /*******************************************************************
     Xml Editor Status bar
     *******************************************************************/

    var layout = require("./layout");
    var dom = require("ace/lib/dom");
    var consoleEl = dom.createElement("div");
    container.parentNode.appendChild(consoleEl);
    var cmdLine = new layout.singleLineEditor(consoleEl);
    var StatusBar = require("ace/ext/statusbar").StatusBar;
    new StatusBar(envXmlContainer.editor, cmdLine.container);


})