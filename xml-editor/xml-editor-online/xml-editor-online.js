define(function (require) {
    "use strict";

    // var alert  = require("ace/alert-node");
    // var fs = require ('fs');

    /************************Environment object of xml editor*************************************/

    // localStorage.clear()

    let errorOccured=false;

    let userEnterUrl=window.location.href;
    var parts = userEnterUrl.split('=', 2);
    var userEnterKey  = parts[1];

    let mode='xml';
    let renderConatentResult="";



    if(userEnterKey !== undefined)
    {
        //'354c99f8a71c64036273abfd7a2ce7a7-ZXz3xa'


        var url = "https://xmleditoronline.org/rest/api.php?request=call";
        var key=[]

        $.ajax('https://xmleditoronline.org/rest/api.php?request=call', {
            type: 'POST',
            data: { key: userEnterKey },
            success: function (data, status, xhr) {

                let decodeData=atob(data.payload.content)
                envXmlContainer.editor.setValue(decodeData)


            },
            error: function (jqXhr, textStatus, errorMessage) {
                $('p').append('Error' + errorMessage);
            }
        });


    }


    var envXmlContainer = {};
    /************************Environment object of Result editor*************************************/
    var envResultContainer = {};

    /************************Environment object of Result editor*************************************/


    /************************Theme for editor*************************************/
    var theme = require("ace/theme/textmate");

    var Split = require("ace/split").Split;
    var UndoManager = require("ace/undomanager").UndoManager;

    /****************************************************************
     create beautifier editor
     ****************************************************************/

    var xmlBeautifier = document.getElementById("xml-beautifier");
    var splitBeautifierContainer = new Split(xmlBeautifier);
    envResultContainer.editor = splitBeautifierContainer.getEditor(0);
    envResultContainer.editor.setReadOnly(true);

    /****************************************************************
     create xml editor
     ****************************************************************/
    var container = document.getElementById("xml-code-editor");
    var split = new Split(container);
    envXmlContainer.editor = split.getEditor(0);


    envXmlContainer.editor.session.setUndoManager(new UndoManager())

            var decodeDataUserInput=localStorage.getItem('data')
            if(decodeDataUserInput !=null) {
            var encodeDataUserInput=atob(decodeDataUserInput)
            envResultContainer.editor.setValue('')
            envXmlContainer.editor.setValue(encodeDataUserInput)
              }



    if(userEnterKey == undefined && decodeDataUserInput==null) {
        envResultContainer.editor.setValue(

            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
            "<query xmlns:yahoo=\"http://www.yahooapis.com/v1/base.rng\"\n" +
            "       yahoo:count=\"7\" yahoo:created=\"2011-10-11T08:40:23Z\" yahoo:lang=\"en-US\">\n" +
            "    <diagnostics>\n" +
            "        <publiclyCallable>true</publiclyCallable>\n" +
            "        <url execution-start-time=\"0\" execution-stop-time=\"25\" execution-time=\"25\"><![CDATA[http://where.yahooapis.com/v1/continents;start=0;count=10]]></url>\n" +
            "        <user-time>26</user-time>\n" +
            "        <service-time>25</service-time>\n" +
            "        <build-version>21978</build-version>\n" +
            "    </diagnostics>\n" +
            "    <results>\n" +
            "        <place xmlns=\"http://where.yahooapis.com/v1/schema.rng\"\n" +
            "               xml:lang=\"en-US\" yahoo:uri=\"http://where.yahooapis.com/v1/place/24865670\">\n" +
            "            <woeid>24865670</woeid>\n" +
            "            <placeTypeName code=\"29\">Continent</placeTypeName>\n" +
            "            <name>Africa</name>\n" +
            "        </place>\n" +
            "        <place xmlns=\"http://where.yahooapis.com/v1/schema.rng\"\n" +
            "               xml:lang=\"en-US\" yahoo:uri=\"http://where.yahooapis.com/v1/place/24865675\">\n" +
            "            <woeid>24865675</woeid>\n" +
            "            <placeTypeName code=\"29\">Continent</placeTypeName>\n" +
            "            <name>Europe</name>\n" +
            "        </place>\n" +
            "        <place xmlns=\"http://where.yahooapis.com/v1/schema.rng\"\n" +
            "               xml:lang=\"en-US\" yahoo:uri=\"http://where.yahooapis.com/v1/place/24865673\">\n" +
            "            <woeid>24865673</woeid>\n" +
            "            <placeTypeName code=\"29\">Continent</placeTypeName>\n" +
            "            <name>South America</name>\n" +
            "        </place>\n" +
            "        <place xmlns=\"http://where.yahooapis.com/v1/schema.rng\"\n" +
            "               xml:lang=\"en-US\" yahoo:uri=\"http://where.yahooapis.com/v1/place/28289421\">\n" +
            "            <woeid>28289421</woeid>\n" +
            "            <placeTypeName code=\"29\">Continent</placeTypeName>\n" +
            "            <name>Antarctic</name>\n" +
            "        </place>\n" +
            "        <place xmlns=\"http://where.yahooapis.com/v1/schema.rng\"\n" +
            "               xml:lang=\"en-US\" yahoo:uri=\"http://where.yahooapis.com/v1/place/24865671\">\n" +
            "            <woeid>24865671</woeid>\n" +
            "            <placeTypeName code=\"29\">Continent</placeTypeName>\n" +
            "            <name>Asia</name>\n" +
            "        </place>\n" +
            "        <place xmlns=\"http://where.yahooapis.com/v1/schema.rng\"\n" +
            "               xml:lang=\"en-US\" yahoo:uri=\"http://where.yahooapis.com/v1/place/24865672\">\n" +
            "            <woeid>24865672</woeid>\n" +
            "            <placeTypeName code=\"29\">Continent</placeTypeName>\n" +
            "            <name>North America</name>\n" +
            "        </place>\n" +
            "        <place xmlns=\"http://where.yahooapis.com/v1/schema.rng\"\n" +
            "               xml:lang=\"en-US\" yahoo:uri=\"http://where.yahooapis.com/v1/place/55949070\">\n" +
            "            <woeid>55949070</woeid>\n" +
            "            <placeTypeName code=\"29\">Continent</placeTypeName>\n" +
            "            <name>Australia</name>\n" +
            "        </place>\n" +
            "    </results>\n" +
            "</query>"

        )

        envXmlContainer.editor.setValue(

            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
            "<query xmlns:yahoo=\"http://www.yahooapis.com/v1/base.rng\"\n" +
            "       yahoo:count=\"7\" yahoo:created=\"2011-10-11T08:40:23Z\" yahoo:lang=\"en-US\">\n" +
            "    <diagnostics>\n" +
            "        <publiclyCallable>true</publiclyCallable>\n" +
            "        <url execution-start-time=\"0\" execution-stop-time=\"25\" execution-time=\"25\"><![CDATA[http://where.yahooapis.com/v1/continents;start=0;count=10]]></url>\n" +
            "        <user-time>26</user-time>\n" +
            "        <service-time>25</service-time>\n" +
            "        <build-version>21978</build-version>\n" +
            "    </diagnostics>\n" +
            "    <results>\n" +
            "        <place xmlns=\"http://where.yahooapis.com/v1/schema.rng\"\n" +
            "               xml:lang=\"en-US\" yahoo:uri=\"http://where.yahooapis.com/v1/place/24865670\">\n" +
            "            <woeid>24865670</woeid>\n" +
            "            <placeTypeName code=\"29\">Continent</placeTypeName>\n" +
            "            <name>Africa</name>\n" +
            "        </place>\n" +
            "        <place xmlns=\"http://where.yahooapis.com/v1/schema.rng\"\n" +
            "               xml:lang=\"en-US\" yahoo:uri=\"http://where.yahooapis.com/v1/place/24865675\">\n" +
            "            <woeid>24865675</woeid>\n" +
            "            <placeTypeName code=\"29\">Continent</placeTypeName>\n" +
            "            <name>Europe</name>\n" +
            "        </place>\n" +
            "        <place xmlns=\"http://where.yahooapis.com/v1/schema.rng\"\n" +
            "               xml:lang=\"en-US\" yahoo:uri=\"http://where.yahooapis.com/v1/place/24865673\">\n" +
            "            <woeid>24865673</woeid>\n" +
            "            <placeTypeName code=\"29\">Continent</placeTypeName>\n" +
            "            <name>South America</name>\n" +
            "        </place>\n" +
            "        <place xmlns=\"http://where.yahooapis.com/v1/schema.rng\"\n" +
            "               xml:lang=\"en-US\" yahoo:uri=\"http://where.yahooapis.com/v1/place/28289421\">\n" +
            "            <woeid>28289421</woeid>\n" +
            "            <placeTypeName code=\"29\">Continent</placeTypeName>\n" +
            "            <name>Antarctic</name>\n" +
            "        </place>\n" +
            "        <place xmlns=\"http://where.yahooapis.com/v1/schema.rng\"\n" +
            "               xml:lang=\"en-US\" yahoo:uri=\"http://where.yahooapis.com/v1/place/24865671\">\n" +
            "            <woeid>24865671</woeid>\n" +
            "            <placeTypeName code=\"29\">Continent</placeTypeName>\n" +
            "            <name>Asia</name>\n" +
            "        </place>\n" +
            "        <place xmlns=\"http://where.yahooapis.com/v1/schema.rng\"\n" +
            "               xml:lang=\"en-US\" yahoo:uri=\"http://where.yahooapis.com/v1/place/24865672\">\n" +
            "            <woeid>24865672</woeid>\n" +
            "            <placeTypeName code=\"29\">Continent</placeTypeName>\n" +
            "            <name>North America</name>\n" +
            "        </place>\n" +
            "        <place xmlns=\"http://where.yahooapis.com/v1/schema.rng\"\n" +
            "               xml:lang=\"en-US\" yahoo:uri=\"http://where.yahooapis.com/v1/place/55949070\">\n" +
            "            <woeid>55949070</woeid>\n" +
            "            <placeTypeName code=\"29\">Continent</placeTypeName>\n" +
            "            <name>Australia</name>\n" +
            "        </place>\n" +
            "    </results>\n" +
            "</query>"

        )
    }



    //
    // envXmlContainer.editor.session.setUseWrapMode(true);
    // envResultContainer.editor.session.setUseWrapMode(true);


    // envResultContainer.editor.renderer.$cursorLayer.showCursor()


    /**********************************************************************************
     Beautifier code and xml code set by Default start and mode set to xml
     **********************************************************************************/

    // var decode=localStorage.getItem('data');
    // var decodedString = atob(decode);




    envXmlContainer.editor.session.setMode("ace/mode/xml")
    envResultContainer.editor.session.setMode("ace/mode/xml")



    /*******************************************************************
     Xml to Minified Function Work
     *******************************************************************/

    var xmlTreeMinifiedEditor = document.getElementById("minifiedButton")

    xmlTreeMinifiedEditor.onclick = function (e) {
        envResultContainer.editor.renderer.setShowGutter(true);
        envResultContainer.editor.session.setMode("ace/mode/xml")
        var totalLineNumber=envXmlContainer.editor.session.getLength();


        var result  = envXmlContainer.editor.getValue();

        if(result == "") {
            envResultContainer.editor.setValue('')

            var x =  document.getElementsByClassName('xmltree');
            var i;
            for (i = 0; i < x.length; i++) {
                x[i].style.display = 'none';
            }
            return
        }

        var minifiedXml = result.replace(/\n|\r/g, " ");
        var removeSpaces=minifiedXml.replace(/>\s{0,}</g,"><");

        // var minifiedXml = result.replace(/\n|\r/g, " ");
        // var removeSpaces=minifiedXml.replace(/\s/g, '');

        envResultContainer.editor.setValue(removeSpaces)
        document.getElementById("outputHeading").innerHTML='Result: Minified XML'


        var x =  document.getElementsByClassName('xmltree');
        var i;
        for (i = 0; i < x.length; i++) {
            x[i].style.display = 'none';
        }


    }



    /*******************************************************************
     Xml  Minified Function Work
     *******************************************************************/

    var xmlTreeMinifiedEditorSelf = document.getElementById("minifiedButtonXmlEditor")

    xmlTreeMinifiedEditorSelf.onclick = function (e) {
        if(errorOccured==true) {
            document.getElementById("errorMessage").style.display = "block";

        }
        else {
            envXmlContainer.editor.renderer.setShowGutter(true);
            envXmlContainer.editor.session.setMode("ace/mode/xml")
            var totalLineNumber = envXmlContainer.editor.session.getLength();
            var result = envXmlContainer.editor.getValue();

            if (result == "") return

            var minifiedXml = result.replace(/\n|\r/g, " ");
            var removeSpaces = minifiedXml.replace(/>\s{0,}</g, "><");
            // replace(/>\s{0,}</g,"><");

            envXmlContainer.editor.setValue(removeSpaces)
        }

    }


    /*******************************************************************
     Xml to Beautifier Function Work
     *******************************************************************/

    var xmlPlayEditor = document.getElementById("play")
    xmlPlayEditor.onclick = function (e) {

        if(errorOccured==true) {
            document.getElementById("errorMessage").style.display = "block";

        }
        else{
            document.getElementById("errorMessage").style.display = "none";
            envResultContainer.editor.renderer.setShowGutter(true);
            envResultContainer.editor.session.setMode("ace/mode/xml")
            /*
            get the content of editor xml
             */

            var xmlCode = envXmlContainer.editor.getValue()
            if(xmlCode == "") {
                envResultContainer.editor.setValue('')
                var x =  document.getElementsByClassName('xmltree');
                var i;
                for (i = 0; i < x.length; i++) {
                    x[i].style.display = 'none';
                }
                return
            }




            /*
            beautify the content with our js file
             */

            var prettyXmlText = new XmlBeautify().beautify(xmlCode,
                {indent: "    ",useSelfClosingElement: false});

            /*
           set the content after beautify in beautify editor
             */
            envResultContainer.editor.setValue(xmlCode)

            document.getElementById("outputHeading").innerHTML='Result: Output XML'



            var x =  document.getElementsByClassName('xmltree');
            var i;
            for (i = 0; i < x.length; i++) {
                x[i].style.display = 'none';
            }
        }


    }


    /*******************************************************************
     Xml to Beautifier Icon Click Function Work
     *******************************************************************/


    var xmlBeautifierEditor = document.getElementById("beautifierButton")
    var XmlBeautify = require("./XmlBeautify")
    xmlBeautifierEditor.onclick = function (e) {

        envResultContainer.editor.renderer.setShowGutter(true);
        envResultContainer.editor.session.setMode("ace/mode/xml")
        /*
        get the content of editor xml
         */

        // var decode=localStorage.getItem('data');
        // var decodedString = atob(decode);


        var xmlCode = envXmlContainer.editor.getValue()
        if(xmlCode == "") {
            envResultContainer.editor.setValue('')
            var x =  document.getElementsByClassName('xmltree');
            var i;
            for (i = 0; i < x.length; i++) {
                x[i].style.display = 'none';
            }
            return
        }


        /*
        beautify the content with our js file
         */

        var prettyXmlText = new XmlBeautify().beautify(xmlCode,
            {indent: "    ",useSelfClosingElement: false});

        /*
       set the content after beautify in beautify editor
         */
        mode="xml"
        renderConatentResult=prettyXmlText
        envResultContainer.editor.setValue(prettyXmlText)

        document.getElementById("outputHeading").innerHTML='Result : Beautify XML'



        var x =  document.getElementsByClassName('xmltree');
        var i;
        for (i = 0; i < x.length; i++) {
            x[i].style.display = 'none';
        }

    }

    var xmlBeautifierEditorIcon = document.getElementById("beautifierButtonXmlToEditor")


    xmlBeautifierEditorIcon.onclick = function (e) {

        // document.getElementById("beautifierButtonXmlToEditor").style.color='red'


        envResultContainer.editor.renderer.setShowGutter(true);
        envResultContainer.editor.session.setMode("ace/mode/xml")
        /*
        get the content of editor xml
         */

        var xmlCode = envXmlContainer.editor.getValue()
        if(xmlCode == ""){
            envResultContainer.editor.setValue('')
            var x =  document.getElementsByClassName('xmltree');
            var i;
            for (i = 0; i < x.length; i++) {
                x[i].style.display = 'none';
            }
            return
        }


        /*
        beautify the content with our js file
         */

        var prettyXmlText = new XmlBeautify().beautify(xmlCode,
            {indent: "    ",useSelfClosingElement: false});

        /*
       set the content after beautify in beautify editor
         */
        envResultContainer.editor.setValue(prettyXmlText)

        document.getElementById("outputHeading").innerHTML='Result: Beautify XML'

        var x =  document.getElementsByClassName('xmltree');
        var i;
        for (i = 0; i < x.length; i++) {
            x[i].style.display = 'none';
        }

    }


    /*******************************************************************
     Xml  Beautifier Function Work
     *******************************************************************/

    var xmlBeautifierEditorSelf = document.getElementById("beautifierButtonXmlEditor")
    xmlBeautifierEditorSelf.onclick = function (e) {
        if(errorOccured==true) {
            document.getElementById("errorMessage").style.display = "block";

        }
        else{

        envXmlContainer.editor.renderer.setShowGutter(true);
        envXmlContainer.editor.session.setMode("ace/mode/xml")
        /*
        get the content of editor xml
         */

        var xmlCode = envXmlContainer.editor.getValue()


        /*
        beautify the content with our js file
         */
        if (xmlCode == "") return

        var prettyXmlText = new XmlBeautify().beautify(xmlCode,
            {indent: "    ", useSelfClosingElement: false});

        /*
       set the content after beautify in beautify editor
         */
        envXmlContainer.editor.setValue(prettyXmlText)

        // document.getElementById("outputHeading").innerHTML='Result : Beautify XML'


        // var x =  document.getElementsByClassName('xmltree');
        // var i;
        // for (i = 0; i < x.length; i++) {
        //     x[i].style.display = 'none';
        // }
    }

    }


    /*******************************************************************
     Xml Editor Status bar
     *******************************************************************/

        // var layout = require("./layout");
        // var dom = require("ace/lib/dom");
        // var consoleEl= dom.createElement("div")
        // container.parentNode.appendChild(consoleEl);
        // var cmdLine = new layout.singleLineEditor(consoleEl);
        // var StatusBar = require("ace/ext/statusbar").StatusBar;
        // new StatusBar(envXmlContainer.editor, cmdLine.container);


    var StatusBar = require("ace/ext/statusbar").StatusBar;

    new StatusBar(envXmlContainer.editor, document.getElementById("statusBar"));


    /*******************************************************************
     Xml Editor Sample Work
     *******************************************************************/

    var sampleEditor = document.getElementById("sample")

    sampleEditor.onclick = function (e) {

        var x =  document.getElementsByClassName('xmltree');
        var i;
        for (i = 0; i < x.length; i++) {
            x[i].style.display = 'none';
        }


        envResultContainer.editor.renderer.setShowGutter(true);
        document.getElementById("outputHeading").innerHTML='Result: Output XML'
        envResultContainer.editor.session.setMode("ace/mode/xml")
        envXmlContainer.editor.setValue("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
            "<query xmlns:yahoo=\"http://www.yahooapis.com/v1/base.rng\"\n" +
            "       yahoo:count=\"7\" yahoo:created=\"2011-10-11T08:40:23Z\" yahoo:lang=\"en-US\">\n" +
            "    <diagnostics>\n" +
            "        <publiclyCallable>true</publiclyCallable>\n" +
            "        <url execution-start-time=\"0\" execution-stop-time=\"25\" execution-time=\"25\"><![CDATA[http://where.yahooapis.com/v1/continents;start=0;count=10]]></url>\n" +
            "        <user-time>26</user-time>\n" +
            "        <service-time>25</service-time>\n" +
            "        <build-version>21978</build-version>\n" +
            "    </diagnostics>\n" +
            "    <results>\n" +
            "        <place xmlns=\"http://where.yahooapis.com/v1/schema.rng\"\n" +
            "               xml:lang=\"en-US\" yahoo:uri=\"http://where.yahooapis.com/v1/place/24865670\">\n" +
            "            <woeid>24865670</woeid>\n" +
            "            <placeTypeName code=\"29\">Continent</placeTypeName>\n" +
            "            <name>Africa</name>\n" +
            "        </place>\n" +
            "        <place xmlns=\"http://where.yahooapis.com/v1/schema.rng\"\n" +
            "               xml:lang=\"en-US\" yahoo:uri=\"http://where.yahooapis.com/v1/place/24865675\">\n" +
            "            <woeid>24865675</woeid>\n" +
            "            <placeTypeName code=\"29\">Continent</placeTypeName>\n" +
            "            <name>Europe</name>\n" +
            "        </place>\n" +
            "        <place xmlns=\"http://where.yahooapis.com/v1/schema.rng\"\n" +
            "               xml:lang=\"en-US\" yahoo:uri=\"http://where.yahooapis.com/v1/place/24865673\">\n" +
            "            <woeid>24865673</woeid>\n" +
            "            <placeTypeName code=\"29\">Continent</placeTypeName>\n" +
            "            <name>South America</name>\n" +
            "        </place>\n" +
            "        <place xmlns=\"http://where.yahooapis.com/v1/schema.rng\"\n" +
            "               xml:lang=\"en-US\" yahoo:uri=\"http://where.yahooapis.com/v1/place/28289421\">\n" +
            "            <woeid>28289421</woeid>\n" +
            "            <placeTypeName code=\"29\">Continent</placeTypeName>\n" +
            "            <name>Antarctic</name>\n" +
            "        </place>\n" +
            "        <place xmlns=\"http://where.yahooapis.com/v1/schema.rng\"\n" +
            "               xml:lang=\"en-US\" yahoo:uri=\"http://where.yahooapis.com/v1/place/24865671\">\n" +
            "            <woeid>24865671</woeid>\n" +
            "            <placeTypeName code=\"29\">Continent</placeTypeName>\n" +
            "            <name>Asia</name>\n" +
            "        </place>\n" +
            "        <place xmlns=\"http://where.yahooapis.com/v1/schema.rng\"\n" +
            "               xml:lang=\"en-US\" yahoo:uri=\"http://where.yahooapis.com/v1/place/24865672\">\n" +
            "            <woeid>24865672</woeid>\n" +
            "            <placeTypeName code=\"29\">Continent</placeTypeName>\n" +
            "            <name>North America</name>\n" +
            "        </place>\n" +
            "        <place xmlns=\"http://where.yahooapis.com/v1/schema.rng\"\n" +
            "               xml:lang=\"en-US\" yahoo:uri=\"http://where.yahooapis.com/v1/place/55949070\">\n" +
            "            <woeid>55949070</woeid>\n" +
            "            <placeTypeName code=\"29\">Continent</placeTypeName>\n" +
            "            <name>Australia</name>\n" +
            "        </place>\n" +
            "    </results>\n" +
            "</query>")

        envResultContainer.editor.setValue("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
            "<query xmlns:yahoo=\"http://www.yahooapis.com/v1/base.rng\"\n" +
            "       yahoo:count=\"7\" yahoo:created=\"2011-10-11T08:40:23Z\" yahoo:lang=\"en-US\">\n" +
            "    <diagnostics>\n" +
            "        <publiclyCallable>true</publiclyCallable>\n" +
            "        <url execution-start-time=\"0\" execution-stop-time=\"25\" execution-time=\"25\"><![CDATA[http://where.yahooapis.com/v1/continents;start=0;count=10]]></url>\n" +
            "        <user-time>26</user-time>\n" +
            "        <service-time>25</service-time>\n" +
            "        <build-version>21978</build-version>\n" +
            "    </diagnostics>\n" +
            "    <results>\n" +
            "        <place xmlns=\"http://where.yahooapis.com/v1/schema.rng\"\n" +
            "               xml:lang=\"en-US\" yahoo:uri=\"http://where.yahooapis.com/v1/place/24865670\">\n" +
            "            <woeid>24865670</woeid>\n" +
            "            <placeTypeName code=\"29\">Continent</placeTypeName>\n" +
            "            <name>Africa</name>\n" +
            "        </place>\n" +
            "        <place xmlns=\"http://where.yahooapis.com/v1/schema.rng\"\n" +
            "               xml:lang=\"en-US\" yahoo:uri=\"http://where.yahooapis.com/v1/place/24865675\">\n" +
            "            <woeid>24865675</woeid>\n" +
            "            <placeTypeName code=\"29\">Continent</placeTypeName>\n" +
            "            <name>Europe</name>\n" +
            "        </place>\n" +
            "        <place xmlns=\"http://where.yahooapis.com/v1/schema.rng\"\n" +
            "               xml:lang=\"en-US\" yahoo:uri=\"http://where.yahooapis.com/v1/place/24865673\">\n" +
            "            <woeid>24865673</woeid>\n" +
            "            <placeTypeName code=\"29\">Continent</placeTypeName>\n" +
            "            <name>South America</name>\n" +
            "        </place>\n" +
            "        <place xmlns=\"http://where.yahooapis.com/v1/schema.rng\"\n" +
            "               xml:lang=\"en-US\" yahoo:uri=\"http://where.yahooapis.com/v1/place/28289421\">\n" +
            "            <woeid>28289421</woeid>\n" +
            "            <placeTypeName code=\"29\">Continent</placeTypeName>\n" +
            "            <name>Antarctic</name>\n" +
            "        </place>\n" +
            "        <place xmlns=\"http://where.yahooapis.com/v1/schema.rng\"\n" +
            "               xml:lang=\"en-US\" yahoo:uri=\"http://where.yahooapis.com/v1/place/24865671\">\n" +
            "            <woeid>24865671</woeid>\n" +
            "            <placeTypeName code=\"29\">Continent</placeTypeName>\n" +
            "            <name>Asia</name>\n" +
            "        </place>\n" +
            "        <place xmlns=\"http://where.yahooapis.com/v1/schema.rng\"\n" +
            "               xml:lang=\"en-US\" yahoo:uri=\"http://where.yahooapis.com/v1/place/24865672\">\n" +
            "            <woeid>24865672</woeid>\n" +
            "            <placeTypeName code=\"29\">Continent</placeTypeName>\n" +
            "            <name>North America</name>\n" +
            "        </place>\n" +
            "        <place xmlns=\"http://where.yahooapis.com/v1/schema.rng\"\n" +
            "               xml:lang=\"en-US\" yahoo:uri=\"http://where.yahooapis.com/v1/place/55949070\">\n" +
            "            <woeid>55949070</woeid>\n" +
            "            <placeTypeName code=\"29\">Continent</placeTypeName>\n" +
            "            <name>Australia</name>\n" +
            "        </place>\n" +
            "    </results>\n" +
            "</query>")
    }

    /*******************************************************************
     XML Editor cut Work
     *******************************************************************/

    var cutDataOfXml = document.getElementById("cutDataOfXml")

    cutDataOfXml.onclick=function (e) {
        envResultContainer.editor.renderer.setShowGutter(true);

        envXmlContainer.editor.setValue("")
        envResultContainer.editor.setValue("")

        var x =  document.getElementsByClassName('xmltree')
        var i;
        for (i = 0; i < x.length; i++) {
            x[i].style.display = 'none';
        }
    }

    /*******************************************************************
     XML Editor copy Work
     *******************************************************************/

    var copyDataOfXml=  document.getElementById("copyDataOfXml")

    copyDataOfXml.onclick=function (e) {
        // envResultContainer.editor.renderer.setShowGutter(true);
        var sel = envXmlContainer.editor.selection.toJSON();
        envXmlContainer.editor.selectAll();
        envXmlContainer.editor.focus();
        document.execCommand('copy');
        envXmlContainer.editor.selection.fromJSON(sel);
        // alert('Copied to Clipboard')

        document.getElementById("message").style.display = "block";
        setTimeout(function () {
            document.getElementById("message").style.display = "none";
        },2000)
    }

    /*******************************************************************
     Output Editor copy Work
     *******************************************************************/

    var copyDataOfOutputXml=  document.getElementById("copyDataOfOutputXml")

    copyDataOfOutputXml.onclick=function (e) {
        // envResultContainer.editor.renderer.setShowGutter(true);
        var sel = envResultContainer.editor.selection.toJSON();
        envResultContainer.editor.selectAll();
        envResultContainer.editor.focus();
        document.execCommand('copy');
        envResultContainer.editor.selection.fromJSON(sel);
        // alert('Copied to Clipboard')

        document.getElementById("message").style.display = "block";
        setTimeout( () =>{
            document.getElementById("message").style.display = "none";
        },2000)

    }




    function xml2json(xml) {
        var X = {
            toObj: function(xml) {
                var o = {};
                if (xml.nodeType==1) {   // element node ..
                    if (xml.attributes.length)   // element with attributes  ..
                        for (var i=0; i<xml.attributes.length; i++)
                            o["_"+xml.attributes[i].nodeName] = (xml.attributes[i].nodeValue||"").toString();
                    if (xml.firstChild) { // element has child nodes ..
                        var textChild=0, cdataChild=0, hasElementChild=false;
                        for (var n=xml.firstChild; n; n=n.nextSibling) {
                            if (n.nodeType==1) hasElementChild = true;
                            else if (n.nodeType==3 && n.nodeValue.match(/[^ \f\n\r\t\v]/)) textChild++; // non-whitespace text
                            else if (n.nodeType==4) cdataChild++; // cdata section node
                        }
                        if (hasElementChild) {
                            if (textChild < 2 && cdataChild < 2) { // structured element with evtl. a single text or/and cdata node ..
                                X.removeWhite(xml);
                                for (var n=xml.firstChild; n; n=n.nextSibling) {
                                    if (n.nodeType == 3)  // text node
                                        o["__text"] = X.escape(n.nodeValue);
                                    else if (n.nodeType == 4)  // cdata node
                                        o["__cdata"] = X.escape(n.nodeValue);
                                    else if (o[n.nodeName]) {  // multiple occurence of element ..
                                        if (o[n.nodeName] instanceof Array)
                                            o[n.nodeName][o[n.nodeName].length] = X.toObj(n);
                                        else
                                            o[n.nodeName] = [o[n.nodeName], X.toObj(n)];
                                    }
                                    else  // first occurence of element..
                                        o[n.nodeName] = X.toObj(n);
                                }
                            }
                            else { // mixed content
                                if (!xml.attributes.length)
                                    o = X.escape(X.innerXml(xml));
                                else
                                    o["__text"] = X.escape(X.innerXml(xml));
                            }
                        }
                        else if (textChild) { // pure text
                            if (!xml.attributes.length)
                                o = X.escape(X.innerXml(xml));
                            else
                                o["__text"] = X.escape(X.innerXml(xml));
                        }
                        else if (cdataChild) { // cdata
                            if (cdataChild > 1)
                                o = X.escape(X.innerXml(xml));
                            else
                                for (var n=xml.firstChild; n; n=n.nextSibling)
                                    o["__cdata"] = X.escape(n.nodeValue);
                        }
                    }
                    if (!xml.attributes.length && !xml.firstChild) o = null;
                }
                else if (xml.nodeType==9) { // document.node
                    o = X.toObj(xml.documentElement);
                }
                else
                    alert("unhandled node type: " + xml.nodeType);
                return o;
            },
            toJson: function(o, name, ind) {
                var json = name ? ("\""+name+"\"") : "";
                if (o instanceof Array) {
                    for (var i=0,n=o.length; i<n; i++)
                        o[i] = X.toJson(o[i], "", ind+"\t");
                    json += (name?":[":"[") + (o.length > 1 ? ("\n"+ind+"\t"+o.join(",\n"+ind+"\t")+"\n"+ind) : o.join("")) + "]";
                }
                else if (o == null)
                    json += (name&&":") + "null";
                else if (typeof(o) == "object") {
                    var arr = [];
                    for (var m in o)
                        arr[arr.length] = X.toJson(o[m], m, ind+"\t");
                    json += (name?":{":"{") + (arr.length > 1 ? ("\n"+ind+"\t"+arr.join(",\n"+ind+"\t")+"\n"+ind) : arr.join("")) + "}";
                }
                else if (typeof(o) == "string")
                    json += (name&&":") + "\"" + o.toString() + "\"";
                else
                    json += (name&&":") + o.toString();
                return json;
            },
            innerXml: function(node) {
                var s = ""
                if ("innerHTML" in node)
                    s = node.innerHTML;
                else {
                    var asXml = function(n) {
                        var s = "";
                        if (n.nodeType == 1) {
                            s += "<" + n.nodeName;
                            for (var i=0; i<n.attributes.length;i++)
                                s += " " + n.attributes[i].nodeName + "=\"" + (n.attributes[i].nodeValue||"").toString() + "\"";
                            if (n.firstChild) {
                                s += ">";
                                for (var c=n.firstChild; c; c=c.nextSibling)
                                    s += asXml(c);
                                s += "</"+n.nodeName+">";
                            }
                            else
                                s += "/>";
                        }
                        else if (n.nodeType == 3)
                            s += n.nodeValue;
                        else if (n.nodeType == 4)
                            s += "<![CDATA[" + n.nodeValue + "]]>";
                        return s;
                    };
                    for (var c=node.firstChild; c; c=c.nextSibling)
                        s += asXml(c);
                }
                return s;
            },
            escape: function(txt) {
                return txt.replace(/[\\]/g, "\\\\")
                    .replace(/[\"]/g, '\\"')
                    .replace(/[\n]/g, '\\n')
                    .replace(/[\r]/g, '\\r');
            },
            removeWhite: function(e) {
                e.normalize();
                for (var n = e.firstChild; n; ) {
                    if (n.nodeType == 3) {  // text node
                        if (!n.nodeValue.match(/[^ \f\n\r\t\v]/)) { // pure whitespace text node
                            var nxt = n.nextSibling;
                            e.removeChild(n);
                            n = nxt;
                        }
                        else
                            n = n.nextSibling;
                    }
                    else if (n.nodeType == 1) {  // element node
                        X.removeWhite(n);
                        n = n.nextSibling;
                    }
                    else                      // any other node
                        n = n.nextSibling;
                }
                return e;
            }
        };
        if (xml.nodeType == 9) // document node
            xml = xml.documentElement;
        var json = X.toJson(X.toObj(X.removeWhite(xml)), xml.nodeName, "\t");
        return "{\n"  + json.replace(/\t|\n/g, "")  + "\n}";
    }




    var jsonButton=  document.getElementById("jsonButton")
    jsonButton.onclick=function (e) {


        // document.getElementById("beautifierButtonXmlToEditor").disabled = true;


        envResultContainer.editor.renderer.setShowGutter(true);
        envResultContainer.editor.session.setMode("ace/mode/json")
        var strxml = envXmlContainer.editor.getValue()

        if(strxml == "") {
            envResultContainer.editor.setValue('')
            var x =  document.getElementsByClassName('xmltree');
            var i;
            for (i = 0; i < x.length; i++) {
                x[i].style.display = 'none';
            }
            return
        }
        // const parser = new DOMParser();
        // const srcDOM = parser.parseFromString(strxml, "application/xml");

        var xmlDOM = new DOMParser().parseFromString(strxml, 'text/xml');

        var stringText  =xml2json(xmlDOM);

        var jsonText=JSON.parse(stringText)

        var beautifyJson=JSON.stringify(jsonText,null,3)

        // var minified = JSON.stringify(JSON.parse(beautifyJson));

        // envXmlContainer.editor.
        mode="json"
        renderConatentResult=beautifyJson
        envResultContainer.editor.setValue(beautifyJson)



        document.getElementById("outputHeading").innerHTML='Result : XML To JSON'

        // var beautifyJson=JSON.stringify(xml2json(xmlDOM),null,2)
        //
        // envResultContainer.editor.setValue(beautifyJson)



        var x =  document.getElementsByClassName('xmltree');
        var i;
        for (i = 0; i < x.length; i++) {
            x[i].style.display = 'none';
        }


    }

    var xmlTreeCounter=0;
    var previousCode = ""
    var treeButton=  document.getElementById("treeButton")
    treeButton.onclick=function (e) {

        // document.getElementById("copyDataOfOutputXml").style.display = "none";

        var treeCode = envXmlContainer.editor.getValue()

        if(treeCode == "") {
            envResultContainer.editor.setValue('')
            var x =  document.getElementsByClassName('xmltree');
            var i;
            for (i = 0; i < x.length; i++) {
                x[i].style.display = 'none';
            }
            return
        }
        mode="tree"
        renderConatentResult=treeCode
        document.getElementById("outputHeading").innerHTML='Result : XML To Tree'
        envResultContainer.editor.setValue('')


        envResultContainer.editor.renderer.setShowGutter(false);

        if(xmlTreeCounter >0){
            if(previousCode==treeCode){
                new XMLTree({
                    xml: treeCode,
                    subTreeBranches: true,
                    container: '#xml-beautifier',
                })

            }
            else if(previousCode !=treeCode){
                new XMLTree({
                    xml: treeCode,
                    subTreeBranches: true,
                    container: '#xml-beautifier',
                })
            }


        }
        else if(xmlTreeCounter==0){
            new XMLTree({
                xml: treeCode,
                container: '#xml-beautifier',
                subTreeBranches: true,
            })
        }

        var x =  document.getElementsByClassName('xmltree')
        var i;
        for (i = 0; i < x.length; i++) {
            x[i].style.display = 'none';
            x[x.length-1].style.display = 'block';
        }
        xmlTreeCounter++
        previousCode=treeCode



    }


    /*
    XML TO TREE COLLAPSE

     */
    var iconCollapse=  document.getElementById("collapse")
    iconCollapse.onclick=function (e) {
        // document.getElementById("copyDataOfOutputXml").style.display = "none";

        if(mode=='tree') {

            var treeCode = envXmlContainer.editor.getValue()

            if (treeCode == "") {
                envResultContainer.editor.setValue('')
                var x = document.getElementsByClassName('xmltree');
                var i;
                for (i = 0; i < x.length; i++) {
                    x[i].style.display = 'none';
                }
                return
            }

            document.getElementById("outputHeading").innerHTML = 'Result: Minified Tree'
            envResultContainer.editor.setValue('')


            envResultContainer.editor.renderer.setShowGutter(false);


            if (xmlTreeCounter > 0) {

                new XMLTree({
                    xml: renderConatentResult,
                    container: '#xml-beautifier',
                    subTreeBranches: false,


                })

            }
            else if (xmlTreeCounter == 0) {
                new XMLTree({
                    xml: renderConatentResult,
                    container: '#xml-beautifier',
                    subTreeBranches: false,


                })
            }


            var x = document.getElementsByClassName('xmltree')
            var i;
            for (i = 0; i < x.length; i++) {
                x[i].style.display = 'none';
                x[x.length - 1].style.display = 'block';
            }
            xmlTreeCounter++
            previousCode = treeCode

        }
        else if(mode=='xml')
        {

            envResultContainer.editor.renderer.setShowGutter(true);
            envResultContainer.editor.session.setMode("ace/mode/xml")
            var totalLineNumber = envXmlContainer.editor.session.getLength();


            var result = envXmlContainer.editor.getValue();
            let xmlCollapse;

            if (result == "") {
                envResultContainer.editor.setValue('')

                var x = document.getElementsByClassName('xmltree');
                var i;
                for (i = 0; i < x.length; i++) {
                    x[i].style.display = 'none';
                }
                return
            }

            if(renderConatentResult== ""){
                xmlCollapse=result
            }
            else{
                xmlCollapse=renderConatentResult
            }

            var minifiedXml = xmlCollapse.replace(/\n|\r/g, " ");
            var removeSpaces = minifiedXml.replace(/>\s{0,}</g, "><");

            // var minifiedXml = result.replace(/\n|\r/g, " ");
            // var removeSpaces=minifiedXml.replace(/\s/g, '');

            envResultContainer.editor.setValue(removeSpaces)
            document.getElementById("outputHeading").innerHTML = 'Result: Minified XML'


            var x = document.getElementsByClassName('xmltree');
            var i;
            for (i = 0; i < x.length; i++) {
                x[i].style.display = 'none';
            }


        }
        else if(mode=='json') {
            document.getElementById("outputHeading").innerHTML = 'Result: Minified JSON'
            let minfiedJson=JSON.stringify(JSON.parse(renderConatentResult));
            envResultContainer.editor.setValue(minfiedJson)
        }

        }




    /*
    XML TO TREE Expand

     */
    var iconExpand=  document.getElementById("expand")
    iconExpand.onclick=function (e) {
       if(mode=='tree') {
           var treeCode = envXmlContainer.editor.getValue()

           if (treeCode == "") {
               envResultContainer.editor.setValue('')
               var x = document.getElementsByClassName('xmltree');
               var i;
               for (i = 0; i < x.length; i++) {
                   x[i].style.display = 'none';
               }
               return
           }

           document.getElementById("outputHeading").innerHTML = 'Result: Expand Tree'
           envResultContainer.editor.setValue('')


           envResultContainer.editor.renderer.setShowGutter(false);


           if (xmlTreeCounter > 0) {

               new XMLTree({
                   xml: renderConatentResult,
                   subTreeBranches: true,
                   container: '#xml-beautifier',


               })

           }
           else if (xmlTreeCounter == 0) {
               new XMLTree({
                   xml: renderConatentResult,
                   subTreeBranches: true,
                   container: '#xml-beautifier',
               })
           }


           var x = document.getElementsByClassName('xmltree')
           var i;
           for (i = 0; i < x.length; i++) {
               x[i].style.display = 'none';
               x[x.length - 1].style.display = 'block';
           }
           xmlTreeCounter++
           previousCode = treeCode

       }
       else if(mode =='json'){

           envResultContainer.editor.renderer.setShowGutter(true);
           envResultContainer.editor.session.setMode("ace/mode/json")
           var strxml = envXmlContainer.editor.getValue()

           if(strxml == "") {
               envResultContainer.editor.setValue('')
               var x =  document.getElementsByClassName('xmltree');
               var i;
               for (i = 0; i < x.length; i++) {
                   x[i].style.display = 'none';
               }
               return
           }
           // const parser = new DOMParser();
           // const srcDOM = parser.parseFromString(strxml, "application/xml");

           envResultContainer.editor.setValue(renderConatentResult)



           document.getElementById("outputHeading").innerHTML='Result: Expand JSON'

           // var beautifyJson=JSON.stringify(xml2json(xmlDOM),null,2)
           //
           // envResultContainer.editor.setValue(beautifyJson)



           var x =  document.getElementsByClassName('xmltree');
           var i;
           for (i = 0; i < x.length; i++) {
               x[i].style.display = 'none';
           }


       }
       else if(mode=='xml') {

           envResultContainer.editor.renderer.setShowGutter(true);
           envResultContainer.editor.session.setMode("ace/mode/xml")
           /*
           get the content of editor xml
            */

           var xmlCode = envXmlContainer.editor.getValue()
           let xmlExpand;
           if(xmlCode == ""){
               envResultContainer.editor.setValue('')
               var x =  document.getElementsByClassName('xmltree');
               var i;
               for (i = 0; i < x.length; i++) {
                   x[i].style.display = 'none';
               }
               return
           }

           if(renderConatentResult== ""){
               xmlExpand=xmlCode
           }
           else{
               xmlExpand=renderConatentResult
           }


           /*
           beautify the content with our js file
            */

           var prettyXmlText = new XmlBeautify().beautify(xmlExpand,
               {indent: "    ",useSelfClosingElement: false});

           /*
          set the content after beautify in beautify editor
            */
           envResultContainer.editor.setValue(prettyXmlText)

           document.getElementById("outputHeading").innerHTML='Result: Expand XML'

           var x =  document.getElementsByClassName('xmltree');
           var i;
           for (i = 0; i < x.length; i++) {
               x[i].style.display = 'none';
           }

       }


    }




    // const newurl = window.location.protocol + "//" + window.location.host + window.location.pathname ;
    // window.history.pushState({path:newurl},'',newurl);

    var saveButton =  document.getElementById("saveButton")


    saveButton.onclick=function () {
        let userKey=window.location.href;
        var keyGet = userKey.split('=', 2);
        var finalkey  = keyGet[1];



        let newurl ;

        var strxml = envXmlContainer.editor.getValue()

       
        var encodedString = btoa(strxml);
       


        var url = "https://xmleditoronline.org/rest/api.php?request=call";


        if(finalkey !== undefined)
        {

            $.ajax('https://xmleditoronline.org/rest/api.php?request=call', {
                type: 'POST',  // http method
                data: { xml: encodedString ,
                        key :finalkey
                },  // data to submit
                success: function (data, status, xhr) {
                    document.getElementById("saveMessage").style.display = "block";
                    setTimeout(function () {
                        document.getElementById("saveMessage").style.display = "none";
                    },2000)
                    

                    if (history.pushState) {
                        newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?id='+finalkey;
                        window.history.pushState({path:newurl},'',newurl);

                    }

                },
                error: function (jqXhr, textStatus, errorMessage) {
                    $('p').append('Error' + errorMessage);
                }
            });
        }
        else {

            $.ajax('https://xmleditoronline.org/rest/api.php?request=call', {
                type: 'POST',
                data: {xml: encodedString},
                success: function (data, status, xhr) {
                    document.getElementById("saveMessage").style.display = "block";
                    setTimeout(function () {
                        document.getElementById("saveMessage").style.display = "none";
                    },2000)

                    if (history.pushState) {
                        newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?id=' + data.payload.key;
                        window.history.pushState({path: newurl}, '', newurl);

                    }

                },
                error: function (jqXhr, textStatus, errorMessage) {
                    $('p').append('Error' + errorMessage);
                }
            });
        }









    }

    var downloadButton =document.getElementById("download")
    downloadButton.onclick=function (filename, text) {


        var element = document.createElement('a');
        element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(envResultContainer.editor.getValue()));
        element.setAttribute('download', 'xmleditoronline');

        element.style.display = 'none';
        document.body.appendChild(element);

        element.click();

        document.body.removeChild(element);


    }




    /*
        Undo Work Xml editor
     */

    var undo = document.getElementById('undo')
    undo.onclick=function () {

        envXmlContainer.editor.undo()
    }

    /*
    Redo Work Xml editor
   */


    var redo = document.getElementById('redo')
    redo.onclick=function () {

        envXmlContainer.editor.redo()
    }

    envXmlContainer.editor.session.on('change', function() {
            var encodeData = envXmlContainer.editor.getValue()
            var decodeData =btoa(encodeData)
            localStorage.setItem("data",decodeData );

        })


    envXmlContainer.editor.session.on("changeAnnotation", function () {

        var annot = envXmlContainer.editor.session.getAnnotations();

        if(annot.length == 0 ) {
            document.getElementById("errorMessage").style.display = "none";
            errorOccured=false;
            return;
        }

        // $('errorMessage').html('<span>Test</span>');


        for (var key in annot) {

            if (annot.hasOwnProperty(key))
                // console.log(annot[key].text + "on line " + " " + annot[key].row);
                errorOccured=true;
          document.getElementById("errorMessage").innerHTML = (annot[key].text + "on line " + " " + annot[key].row)
        }
    });






})



