Running Ace
-----------

After the checkout Ace works out of the box. No build step is required. To try it out, simply start the bundled mini HTTP server using Node.JS

```bash
node ./static.js
```

The editor can then be opened at http://localhost:8888/kitchen-sink.html. 


Building Ace
-----------

```bash
npm install
node ./build.js
```



To generate all the files in the ace-builds repository, run `node build.js xml-editor --target ../build`

